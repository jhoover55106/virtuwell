package com.myapp

class CartServiceTests extends GroovyTestCase {

    def cartService

    void testAddProductToCart() {
        def volume = 3
        def cart = Cart.build()
        def product = Product.build()
        cartService.addProductToCart(cart.id, product.id, volume)
        def cartProducts = CartProduct.findAllByCart(cart)
        assert 1 == cartProducts.size()
        assert volume == cartProducts[0].howMany
        cartService.addProductToCart(cart.id, product.id, volume)
        cartProducts = CartProduct.findAllByCart(cart)
        assert 1 == cartProducts.size()
        assert volume * 2 == cartProducts[0].howMany

    }

    void testEmptyCart() {
        def volume = 3
        def cart = Cart.build()
        def product = Product.build()
        cartService.addProductToCart(cart.id, product.id, volume)
        cartService.emptyCart(cart.id)
        def cartProducts = CartProduct.findAllByCart(cart)
        assert 0 == cartProducts.size()
    }

    void testPurchaseCart() {
        def volume = 3
        def price = 1.99
        def shipAddress = 'timbuktu'
        def cart = Cart.build()
        def product = Product.build(price:price)
        def user = User.build()
        def paymentType = PaymentType.build()
        cartService.addProductToCart(cart.id, product.id, volume)
        cartService.purchaseCart(cart.id, user.id, paymentType.id, shipAddress)
        def customerOrders = CustomerOrder.findAllByUser(user)
        assert 1 == customerOrders.size()
        def customerOrder = customerOrders[0]
        assert shipAddress == customerOrder.shipAddress
        assert paymentType == customerOrder.paymentType
        assert paymentType == customerOrder.paymentType
        assert volume * price == customerOrder.totalPrice
        assertNotNull customerOrder.orderDate
        def customerOrderProducts = CustomerOrderProduct.findAllByCustomerOrder(customerOrder)
        assert 1 == customerOrderProducts.size()
        def customerOrderProduct = customerOrderProducts[0]
        assert volume == customerOrderProduct.howMany
    }

    void testGetCartTotalPrice() {
        def volume = 3
        def price = 1.99
        def cart = Cart.build()
        def product = Product.build(price:price)
        cartService.addProductToCart(cart.id, product.id, volume)
        def totalPrice = cartService.getCartTotalPrice(cart)
        assert volume * price == totalPrice

    }
}

