package com.myapp

import groovy.mock.interceptor.MockFor
import org.junit.*
import grails.test.mixin.*

@TestFor(CartController)
@Mock(Cart)
class CartControllerTests {

    @Before
    void setUp() {
    }

    void testIndex() {
        controller.index()
        assert "/cart/show" == response.redirectedUrl
    }

    void testShow() {
        def cartServiceMock = mockFor(CartService)
        cartServiceMock.demand.getCartProducts(1) {id ->}
        cartServiceMock.demand.getCartTotalPrice(1) {cart ->}
        controller.cartService = cartServiceMock.createMock()
        controller.show()
        cartServiceMock.verify()
    }

}
