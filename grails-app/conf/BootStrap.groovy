import com.myapp.PaymentType
import com.myapp.Product
import com.myapp.Role
import com.myapp.User
import com.myapp.UserRole

class BootStrap {

    def init = { servletContext ->

        def testUser = new User(username: 'user', password: 'user',enabled: true)
        testUser.save(flush: true, failOnError: true)

        def adminUser = new User(username: 'admin', password: 'admin', enabled: true)
        adminUser.save(flush: true, failOnError: true)

        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        UserRole.create adminUser, adminRole, true
        UserRole.create testUser, userRole, true

        new Product(name: 'Life Enhancer', price: 100.0).save(flush: true, failOnError: true)
        new Product(name: 'Hair Grower', price: 90.0).save(flush: true, failOnError: true)
        new Product(name: 'Pet Rock', price: 1.5).save(flush: true, failOnError: true)

        new PaymentType(name: 'Credit Card').save(flush: true, failOnError: true)
        new PaymentType(name: 'Check').save(flush: true, failOnError: true)
        new PaymentType(name: 'Paypal').save(flush: true, failOnError: true)
        new PaymentType(name: 'Paid in bananas').save(flush: true, failOnError: true)

    }

    def destroy = {
    }
}
