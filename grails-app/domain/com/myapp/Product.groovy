package com.myapp

class Product {

    String name

    Double price

    static constraints = {
    }

    String toString() {
        "$name ${String.format('$%,8.2f', price)}"
    }
}
