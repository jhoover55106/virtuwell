package com.myapp

class Cart {

    static belongsTo = [user: User]

    static constraints = {
        user(nullable: true)
    }
}
