package com.myapp

class CustomerOrder {

    PaymentType paymentType
    Date orderDate
    String shipAddress
    Double totalPrice

    static belongsTo = [user: User]

    static constraints = {
        shipAddress(blank: false)
    }
}
