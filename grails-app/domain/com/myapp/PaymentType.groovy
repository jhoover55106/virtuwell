package com.myapp

class PaymentType {

    String name

    static constraints = {
    }

    String toString() {
        "$name"
    }

}
