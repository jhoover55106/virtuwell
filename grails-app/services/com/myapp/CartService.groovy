package com.myapp

class CartService {

    def addProductToCart(cartId, productId, volume) {
        def cart = Cart.get(cartId)
        def product = Product.get(productId)
        def cartProducts = CartProduct.findAllByCartAndProduct(cart, product)
        def cartProduct
        if (cartProducts) {
            cartProduct = cartProducts[0]
            cartProduct.howMany += volume
        } else {
            cartProduct = new CartProduct(cart: cart, product: product, howMany: volume)
        }
        cartProduct.save(failOnError: true)
    }

    def emptyCart(cartId) {
        CartProduct.executeUpdate("delete CartProduct cp where cp.cart=?",
                              [Cart.load(cartId)])
    }

    def purchaseCart(cartId, userId, paymentTypeId, shipAddress) {
        def cart = Cart.get(cartId)
        def customerOrder = new CustomerOrder(user: User.load(userId),
                paymentType: PaymentType.load(paymentTypeId),
                shipAddress: shipAddress,
                totalPrice: getCartTotalPrice(cart),
                orderDate: new Date())
        customerOrder.save(failOnError: true, flush: true)
        def cartProducts = CartProduct.findAllByCart(cart)
        cartProducts.each {cartProduct ->
             new CustomerOrderProduct(customerOrder: customerOrder, product: cartProduct.product, howMany: cartProduct.howMany).save(failOnError: true, flush: true)
        }
    }

    def getCartTotalPrice(Cart cart) {
        Double totalPrice = 0
        def cartProducts = CartProduct.findAllByCart(cart)
        cartProducts.each {cartProduct ->
            totalPrice += (cartProduct.product.price * cartProduct.howMany)
        }
        totalPrice
    }

    def getCartProducts(cartId) {
        def cart = Cart.get(cartId)
        def cartProducts = CartProduct.findAllByCart(cart)
        cartProducts
    }


}
