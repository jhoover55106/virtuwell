<%--
  Created by IntelliJ IDEA.
  User: jhoover
  Date: 2/12/15
  Time: 10:00 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta name="layout" content="main">
	<title>Logout</title>
</head>
<body>
    <g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
    </g:if>
       You have logged out.
</body>
</html>