
<%@ page import="com.myapp.Product" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="list-product" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="name" title="${message(code: 'product.name.label', default: 'Name')}" />
						<g:sortableColumn property="price" title="${message(code: 'product.price.label', default: 'Price')}" />
						<g:sortableColumn property="volume" title=" " />
						<g:sortableColumn property="submit" title=" " />
					</tr>
				</thead>
				<tbody>
				<g:each in="${productInstanceList}" status="i" var="productInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
                        <td><g:link action="show" id="${productInstance.id}">${fieldValue(bean: productInstance, field: "name")}</g:link></td>
                 		<td>${String.format('$%,8.2f', productInstance.price)}</td>
                        <g:form>
                          <input type="hidden" name="id" value="${productInstance.id}"/>
                          <td>
                              <g:field name="howMany" type="number" required="true" min="1"/>
                          <td>
                          <td><span class="button"><g:actionSubmit class="button-link" value="Add to Cart" action="AddToCart"></g:actionSubmit> </span><td>
                        </g:form>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${productInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
