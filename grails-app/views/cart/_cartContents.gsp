<table>
     <thead>
         <tr>
             <td>Product<td/>
             <td>Volume<td/>
         </tr>
     </thead>
     <tbody>
     <g:each in="${cartProducts}" status="i" var="cartProduct">
         <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
         <td>${cartProduct.product}</td>
         <td>${cartProduct.howMany}</td>
         </tr>
     </g:each>
     </tbody>
 </table>
