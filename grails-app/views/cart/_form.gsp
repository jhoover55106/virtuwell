<%@ page import="com.myapp.Cart" %>



<div class="fieldcontain ${hasErrors(bean: cartInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="cart.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${com.myapp.User.list()}" optionKey="id" required="" value="${cartInstance?.user?.id}" class="many-to-one"/>
</div>

