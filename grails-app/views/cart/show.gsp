
<%@ page import="com.myapp.Cart" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cart.label', default: 'Cart')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-cart" class="content scaffold-show" role="main">
			<h1>Cart</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

            <g:if test="${cartProducts}">
                <g:render template="cartContents"/>
                <h1>Total: ${totalCost}</h1>
                <g:form>
                    <fieldset class="buttons">
                        <g:hiddenField name="id" value="${cartInstance?.id}" />
                        <g:actionSubmit class="button-link" action="purchase" value="Buy It!"  />
                    </fieldset>
                </g:form>
              </g:if>
             <g:else>
                 <h1>Your cart is empty.</h1>
             </g:else>
		</div>
	</body>
</html>
