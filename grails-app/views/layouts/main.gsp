<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
    <h1 style="margin-left: 15px; font-weight: bold;">Fun Shop</h1>
    <div class="navbar">
        <span><a class="button-link" href="${createLinkTo(dir: '/')}">Home</a></span>
        <span><a class="button-link" href="${createLink(controller:"product")}">Shop</a></span>
        <span><a class="button-link" href="${createLink (controller:"cart", action: "show")}">Cart</a></span>
        <span><a class="button-link" href="${createLink (controller:"customerOrder", action:"create")}">Order</a ></span>
        <span><a class="button-link" href="${createLink (controller:"customerOrder", action:"list")}">History</a ></span>
        <sec:ifLoggedIn>
            <span><a class="button-link" href="${createLink (controller:"logout")}">Logout</a></span>
        </sec:ifLoggedIn>
        <sec:ifNotLoggedIn>
            <span><a class="button-link" href="${createLink (controller:"login", action:"auth")}">Login</a></span>
            <span><a class="button-link" href="${createLink (controller:"register")}">Register</a></span>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <span style="float:right; padding-right: 25px;">You are: <sec:loggedInUserInfo field="username"/></span>
        </sec:ifLoggedIn>
    </div>
    <g:layoutBody/>
		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<g:javascript library="application"/>
		<r:layoutResources />
	</body>
</html>
