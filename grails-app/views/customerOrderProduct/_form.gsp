<%@ page import="com.myapp.CustomerOrderProduct" %>



<div class="fieldcontain ${hasErrors(bean: customerOrderProductInstance, field: 'customerOrder', 'error')} required">
	<label for="customerOrder">
		<g:message code="customerOrderProduct.customerOrder.label" default="Customer Order" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="customerOrder" name="customerOrder.id" from="${com.myapp.CustomerOrder.list()}" optionKey="id" required="" value="${customerOrderProductInstance?.customerOrder?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerOrderProductInstance, field: 'howMany', 'error')} required">
	<label for="howMany">
		<g:message code="customerOrderProduct.howMany.label" default="How Many" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="howMany" type="number" value="${customerOrderProductInstance.howMany}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerOrderProductInstance, field: 'product', 'error')} required">
	<label for="product">
		<g:message code="customerOrderProduct.product.label" default="Product" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="product" name="product.id" from="${com.myapp.Product.list()}" optionKey="id" required="" value="${customerOrderProductInstance?.product?.id}" class="many-to-one"/>
</div>

