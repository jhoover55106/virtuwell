
<%@ page import="com.myapp.CustomerOrder" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'customerOrder.label', default: 'CustomerOrder')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-customerOrder" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="shipAddress" title="${message(code: 'customerOrder.shipAddress.label', default: 'Ship Address')}" />
					
						<g:sortableColumn property="orderDate" title="${message(code: 'customerOrder.orderDate.label', default: 'Order Date')}" />
					
						<th><g:message code="customerOrder.paymentType.label" default="Payment Type" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${customerOrderInstanceList}" status="i" var="customerOrderInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${customerOrderInstance.id}">${fieldValue(bean: customerOrderInstance, field: "shipAddress")}</g:link></td>
					
						<td><g:formatDate date="${customerOrderInstance.orderDate}" /></td>
					
						<td>${fieldValue(bean: customerOrderInstance, field: "paymentType")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${customerOrderInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
