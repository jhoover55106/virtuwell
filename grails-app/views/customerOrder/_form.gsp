<%@ page import="com.myapp.CustomerOrder" %>



<div class="fieldcontain ${hasErrors(bean: customerOrderInstance, field: 'shipAddress', 'error')} required">
	<label for="shipAddress">
		<g:message code="customerOrder.shipAddress.label" default="Ship Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="shipAddress" required="" value="${customerOrderInstance?.shipAddress}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerOrderInstance, field: 'paymentType', 'error')} required">
	<label for="paymentType">
		<g:message code="customerOrder.paymentType.label" default="Payment Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="paymentType" name="paymentType.id" from="${com.myapp.PaymentType.list()}" optionKey="id" required="" value="${customerOrderInstance?.paymentType?.id}" class="many-to-one"/>
</div>


