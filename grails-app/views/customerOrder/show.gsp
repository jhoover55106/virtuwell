
<%@ page import="com.myapp.CustomerOrder" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'customerOrder.label', default: 'CustomerOrder')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="show-customerOrder" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list customerOrder">
			
				<g:if test="${customerOrderInstance?.shipAddress}">
				<li class="fieldcontain">
					<span id="shipAddress-label" class="property-label"><g:message code="customerOrder.shipAddress.label" default="Ship Address" /></span>
					
						<span class="property-value" aria-labelledby="shipAddress-label"><g:fieldValue bean="${customerOrderInstance}" field="shipAddress"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${customerOrderInstance?.orderDate}">
				<li class="fieldcontain">
					<span id="orderDate-label" class="property-label"><g:message code="customerOrder.orderDate.label" default="Order Date" /></span>
					
						<span class="property-value" aria-labelledby="orderDate-label"><g:formatDate date="${customerOrderInstance?.orderDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${customerOrderInstance?.paymentType}">
				<li class="fieldcontain">
					<span id="paymentType-label" class="property-label"><g:message code="customerOrder.paymentType.label" default="Payment Type" /></span>
					
						<span class="property-value" aria-labelledby="paymentType-label">${customerOrderInstance?.paymentType?.encodeAsHTML()}</span>
					
				</li>
				</g:if>

				<g:if test="${customerOrderInstance?.totalPrice}">
				<li class="fieldcontain">
					<span id="totalPrice-label" class="property-label"><g:message code="customerOrder.totalPrice.label" default="Total" /></span>

						<span class="property-value" aria-labelledby="paymentType-label">${String.format('$%,8.2f', customerOrderInstance.totalPrice).encodeAsHTML()}</span>

				</li>
				</g:if>

                <g:render contextPath="/cart" template="cartContents"/>


			</ol>
		</div>
	</body>
</html>
