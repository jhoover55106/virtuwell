
<%@ page import="com.myapp.CartProduct" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cartProduct.label', default: 'CartProduct')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-cartProduct" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-cartProduct" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="cartProduct.cart.label" default="Cart" /></th>
					
						<g:sortableColumn property="howMany" title="${message(code: 'cartProduct.howMany.label', default: 'How Many')}" />
					
						<th><g:message code="cartProduct.product.label" default="Product" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${cartProductInstanceList}" status="i" var="cartProductInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${cartProductInstance.id}">${fieldValue(bean: cartProductInstance, field: "cart")}</g:link></td>
					
						<td>${fieldValue(bean: cartProductInstance, field: "howMany")}</td>
					
						<td>${fieldValue(bean: cartProductInstance, field: "product")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${cartProductInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
