
<%@ page import="com.myapp.CartProduct" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'cartProduct.label', default: 'CartProduct')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-cartProduct" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-cartProduct" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list cartProduct">
			
				<g:if test="${cartProductInstance?.cart}">
				<li class="fieldcontain">
					<span id="cart-label" class="property-label"><g:message code="cartProduct.cart.label" default="Cart" /></span>
					
						<span class="property-value" aria-labelledby="cart-label"><g:link controller="cart" action="show" id="${cartProductInstance?.cart?.id}">${cartProductInstance?.cart?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${cartProductInstance?.howMany}">
				<li class="fieldcontain">
					<span id="howMany-label" class="property-label"><g:message code="cartProduct.howMany.label" default="How Many" /></span>
					
						<span class="property-value" aria-labelledby="howMany-label"><g:fieldValue bean="${cartProductInstance}" field="howMany"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${cartProductInstance?.product}">
				<li class="fieldcontain">
					<span id="product-label" class="property-label"><g:message code="cartProduct.product.label" default="Product" /></span>
					
						<span class="property-value" aria-labelledby="product-label"><g:link controller="product" action="show" id="${cartProductInstance?.product?.id}">${cartProductInstance?.product?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${cartProductInstance?.id}" />
					<g:link class="edit" action="edit" id="${cartProductInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
