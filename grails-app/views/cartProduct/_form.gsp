<%@ page import="com.myapp.CartProduct" %>



<div class="fieldcontain ${hasErrors(bean: cartProductInstance, field: 'cart', 'error')} required">
	<label for="cart">
		<g:message code="cartProduct.cart.label" default="Cart" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="cart" name="cart.id" from="${com.myapp.Cart.list()}" optionKey="id" required="" value="${cartProductInstance?.cart?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cartProductInstance, field: 'howMany', 'error')} required">
	<label for="howMany">
		<g:message code="cartProduct.howMany.label" default="How Many" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="howMany" type="number" value="${cartProductInstance.howMany}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: cartProductInstance, field: 'product', 'error')} required">
	<label for="product">
		<g:message code="cartProduct.product.label" default="Product" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="product" name="product.id" from="${com.myapp.Product.list()}" optionKey="id" required="" value="${cartProductInstance?.product?.id}" class="many-to-one"/>
</div>

