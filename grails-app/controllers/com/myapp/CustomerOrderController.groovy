package com.myapp

import grails.plugins.springsecurity.Secured

class CustomerOrderController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def cartService
    def springSecurityService

    def index() {
        redirect(action: "list", params: params)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_USER'])
    def list(Integer max) {
        def customerOrderList = CustomerOrder.findAllByUser(springSecurityService.currentUser)
        springSecurityService.currentUser
        params.max = Math.min(max ?: 10, 100)
        [customerOrderInstanceList: customerOrderList, customerOrderInstanceTotal: CustomerOrder.count()]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_USER'])
    def create() {
        def cartInstance
        if (session.cartId) {
            cartInstance = Cart.get(session.cartId)
        } else {
            cartInstance = new Cart()
            cartInstance.save(failOnError: true, flush: true)
            session.cartId = cartInstance.id
        }
        [customerOrderInstance: new CustomerOrder(params), cartProducts:cartService.getCartProducts(cartInstance.id)]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_USER'])
    def save() {
        def customerOrderInstance = new CustomerOrder(params)
        customerOrderInstance.orderDate = new Date()
        customerOrderInstance.user = springSecurityService.currentUser
        customerOrderInstance.totalPrice = 0.0
        if (!customerOrderInstance.validate()) {
            render(view: "create", model: [customerOrderInstance: customerOrderInstance])
            return
        }
        cartService.purchaseCart(session.cartId, springSecurityService.currentUser.id, params.paymentType.id, params.shipAddress)
        cartService.emptyCart(session.cartId)
        redirect(action: "list")
    }

    @Secured(['ROLE_ADMIN', 'ROLE_USER'])
    def show(Long id) {
        def customerOrderInstance = CustomerOrder.get(id)
        if (!customerOrderInstance || customerOrderInstance.user != springSecurityService.currentUser) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerOrder.label', default: 'CustomerOrder'), id])
            redirect(action: "list")
            return
        }

        [customerOrderInstance: customerOrderInstance, cartProducts: CustomerOrderProduct.findAllByCustomerOrder(customerOrderInstance)]
    }

}
