package com.myapp

import org.springframework.dao.DataIntegrityViolationException

class CartController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def cartService

    def index() {
        redirect(action: "show")
    }

    def show() {
        def cartInstance
        if (session.cartId) {
            cartInstance = Cart.get(session.cartId)
        } else {
            cartInstance = new Cart()
            cartInstance.save(failOnError: true, flush: true)
            session.cartId = cartInstance.id

        }
        [cartInstance: cartInstance, cartProducts:cartService.getCartProducts(cartInstance.id), totalCost: cartService.getCartTotalPrice(cartInstance)]
    }

    def purchase() {
        def cartInstance = Cart.get(session.cartId)
        if (!cartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cart.label', default: 'Cart'), id])
            redirect(action: "show")
            return
        }
        redirect(controller: "customerOrder", action: "create")
     }


}
